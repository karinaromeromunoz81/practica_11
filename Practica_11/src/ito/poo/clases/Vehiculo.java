package ito.poo.clases;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.nio.file.*;
import java.nio.charset.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vehiculo implements Serializable {

    private String registroMarca;
    private String modelo;
    private float cantidadMaxCarga;
    private java.time.LocalDate fechaAdquisicion;
    private ArrayList<Viaje> listaViajesRegistrados = new ArrayList<Viaje>();
    private int identificador;

    Vehiculo() {

    }

    Vehiculo(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public float asignarVehiculo() {
        return cantidadMaxCarga;
    }

    public float eliminarVehiculo() {
        return cantidadMaxCarga;
    }

    public Vehiculo(int identificador, String registroMarca, String modelo, float cantidadMaxCarga, LocalDate fechaAdquisicion) {
        this.registroMarca = registroMarca;
        this.modelo = modelo;
        this.cantidadMaxCarga = cantidadMaxCarga;
        this.fechaAdquisicion = fechaAdquisicion;
        this.identificador = identificador;
    }

    Vehiculo(String registroMarca, String modelo, float cantidadMaxCarga, LocalDate fechaAdquisicion, ArrayList<Viaje> listaViajesRegistrados, int identificador) {
        super();
        this.registroMarca = registroMarca;
        this.modelo = modelo;
        this.cantidadMaxCarga = cantidadMaxCarga;
        this.fechaAdquisicion = fechaAdquisicion;
        this.listaViajesRegistrados = listaViajesRegistrados;
        this.identificador = identificador;
    }

    public void guardar(Vehiculo vehiculo) {
        try {
            String ruta = System.getProperty("user.home") + "\\vehiculos\\vehiculos.txt";
            String contenido = vehiculo.getIdentificador() + ","
                    + vehiculo.getRegistroMarca() + ","
                    + vehiculo.getModelo() + ","
                    + vehiculo.getCantidadMaxCarga() + ","
                    + vehiculo.getFechaAdquisicion();
            File file = new File(ruta);

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(contenido);
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList leerArchivo() throws FileNotFoundException, ParseException {
        File doc = new File(System.getProperty("user.home") + "\\vehiculos\\vehiculos.txt");
        ArrayList<String> lines = new ArrayList<>();
        ArrayList<Vehiculo> vehiculos = new ArrayList<>();
        BufferedReader obj = new BufferedReader(new FileReader(doc));

        String strng;
        try {
            while ((strng = obj.readLine()) != null) {
                lines.add(strng);
            }
        } catch (IOException ex) {
            Logger.getLogger(Vehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (String line : lines) {
            String[] data = line.split(",");
            int id = Integer.parseInt((data[0]));
            float cant = Float.parseFloat(data[3]);
            LocalDate date = LocalDate.parse(data[4]);

            vehiculos.add(new Vehiculo(id, data[1], data[2], cant, date));
        }
        return vehiculos;
    }

    @Override
    public String toString() {
        return "Vehiculo [registroMarca=" + registroMarca + ", modelo=" + modelo + ", cantidadMaxCarga="
                + cantidadMaxCarga + ", fechaAdquisicion=" + fechaAdquisicion + ", listaViajesRegistrados="
                + listaViajesRegistrados + ", identificador=" + identificador + "]";
    }

    public String getRegistroMarca() {
        return registroMarca;
    }

    public void setRegistroMarca(String registroMarca) {
        this.registroMarca = registroMarca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getCantidadMaxCarga() {
        return cantidadMaxCarga;
    }

    public void setCantidadMaxCarga(float cantidadMaxCarga) {
        this.cantidadMaxCarga = cantidadMaxCarga;
    }

    public java.time.LocalDate getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(java.time.LocalDate fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public ArrayList<Viaje> getListaViajesRegistrados() {
        return listaViajesRegistrados;
    }

    public void setListaViajesRegistrados(ArrayList<Viaje> listaViajesRegistrados) {
        this.listaViajesRegistrados = listaViajesRegistrados;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(cantidadMaxCarga);
        result = prime * result + ((fechaAdquisicion == null) ? 0 : fechaAdquisicion.hashCode());
        result = prime * result + identificador;
        result = prime * result + ((listaViajesRegistrados == null) ? 0 : listaViajesRegistrados.hashCode());
        result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
        result = prime * result + ((registroMarca == null) ? 0 : registroMarca.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Vehiculo other = (Vehiculo) obj;
        if (Float.floatToIntBits(cantidadMaxCarga) != Float.floatToIntBits(other.cantidadMaxCarga)) {
            return false;
        }
        if (fechaAdquisicion == null) {
            if (other.fechaAdquisicion != null) {
                return false;
            }
        } else if (!fechaAdquisicion.equals(other.fechaAdquisicion)) {
            return false;
        }
        if (identificador != other.identificador) {
            return false;
        }
        if (listaViajesRegistrados == null) {
            if (other.listaViajesRegistrados != null) {
                return false;
            }
        } else if (!listaViajesRegistrados.equals(other.listaViajesRegistrados)) {
            return false;
        }
        if (modelo == null) {
            if (other.modelo != null) {
                return false;
            }
        } else if (!modelo.equals(other.modelo)) {
            return false;
        }
        if (registroMarca == null) {
            if (other.registroMarca != null) {
                return false;
            }
        } else if (!registroMarca.equals(other.registroMarca)) {
            return false;
        }
        return true;
    }

    public int compareTo(Vehiculo arg0) {
        int r = 0;
        if (!this.registroMarca.equals(arg0.getRegistroMarca())) {
            return this.registroMarca.compareTo(arg0.getRegistroMarca());
        } else if (!this.modelo.equals(arg0.getModelo())) {
            return this.modelo.compareTo(arg0.getModelo());
        } else if (this.cantidadMaxCarga != arg0.getCantidadMaxCarga()) {
            return this.cantidadMaxCarga > arg0.getCantidadMaxCarga() ? 1 : -1;
        } else if (!this.fechaAdquisicion.equals(arg0.getFechaAdquisicion())) {
            return this.fechaAdquisicion.compareTo(arg0.getFechaAdquisicion());
        } else if (this.identificador != arg0.getIdentificador()) {
            return this.identificador > arg0.getIdentificador() ? 1 : -1;
        }
        return r;
    }
}