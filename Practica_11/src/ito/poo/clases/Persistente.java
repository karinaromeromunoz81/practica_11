package ito.poo.clases;


public class Persistente {
	
	public static void grabaDispositivos(ArrayList<Vehiculo> lista) {
		ObjectOutputStream file=null;
		try {
			file =new ObjectOutputStream(new FileOutputStream("datos.dat"));
			for(Vehiculo d: lista)
				file.writeObject(d);
			file.close();
		}catch(Exception e) {
			System.err.println(e.getStackTrace());
		}
	}
	
	
	public static ArrayList<Vehiculo> recuperaVehiculos(){
		ArrayList<Vehiculo> l = new ArrayList<Vehiculo>();
		ObjectInputStream file=null;
		Vehiculo d=null;
		try {
			  file = new ObjectInputStream(new FileInputStream("datos.txt"));
			  while((d=(Vehiculo)file.readObject())!=null) {
			    l.add(d);
			  }
		}catch(Exception e) {
			//System.err.println(e.getCause());
		}
		return l;
	}

}
