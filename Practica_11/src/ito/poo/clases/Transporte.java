package ito.poo.clases;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Transporte {

    public static void main(String[] args) {
        Archivo archivo = new Archivo();
        archivo.crearCarpeta();
        archivo.generarArchivo();
        Registro.run();

    }

}