package ito.poo.clases;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Archivo {

    public void crearCarpeta() {
        String fileDir = System.getProperty("user.home") + "\\vehiculos";
        File directorio = new File(fileDir);
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio creado");
            } else {
                System.out.println("Error al crear directorio");
            }
        }
    }

    public void generarArchivo() {
        String fileDir = System.getProperty("user.home") + "\\vehiculos\\vehiculos.txt";

        File archivo = new File(fileDir);
        if (!archivo.exists()) {
            try {
                if (archivo.createNewFile()) {
                    System.out.println("Archivo creado");
                } else {
                    System.out.println("Error al crear archivo");
                }
            } catch (IOException ex) {
                Logger.getLogger(Archivo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}